What you need to do is click on the website given then right click wherever
and click inspect then go to sources and the first bit of flag
should be in the style.css file and the last of the flag in the 
script.js file it should look like this:

style.css:

body {
  background-color: lightblue;
}

/*  picoCTF{xxxxxxxxxxx_xxxx_  */

script.js:

function greetings()
{
  alert("This code is in a separate file!");
}

//  xxx_xxxx_xxxxxxxx}

 
